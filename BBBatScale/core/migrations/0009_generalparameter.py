# Generated by Django 3.0.4 on 2020-03-15 10:01

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0008_jitsiroom_is_password_public'),
    ]

    operations = [
        migrations.CreateModel(
            name='GeneralParameter',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('latest_news', models.TextField(blank=True, null=True)),
            ],
        ),
    ]
